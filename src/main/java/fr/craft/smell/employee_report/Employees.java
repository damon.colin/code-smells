package fr.craft.smell.employee_report;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Employees {

  private static final Pattern NAME_PATTERN = Pattern.compile("(\\p{L}+)(\\P{L}*)");

  private final List<Employee> employees;

  private Employees(EmployeesBuilder builder) {
    employees = builder.employees;
  }

  public static EmployeesBuilder builder() {
    return new EmployeesBuilder();
  }

  public Collection<String> sundayWorkers() {
    return employees
      .stream()
      .filter(employee -> employee.age >= 18)
      .map(Employee::name)
      .map(
        name1 ->
          NAME_PATTERN.matcher(name1.trim())
            .results()
            .map(
              result ->
                result.group(1).substring(0, 1).toUpperCase() +
                result.group(1).substring(1, result.group(1).length()).toLowerCase() +
                result.group(2)
            )
            .collect(Collectors.joining())
      )
      .toList();
  }

  public static class EmployeesBuilder {

    private final List<Employee> employees = new ArrayList<>();

    private EmployeesBuilder() {}

    public EmployeesBuilder add(String name, int age) {
      employees.add(new Employee(name, age));

      return this;
    }

    public Employees build() {
      return new Employees(this);
    }
  }

  private record Employee(String name, int age) {}
}
