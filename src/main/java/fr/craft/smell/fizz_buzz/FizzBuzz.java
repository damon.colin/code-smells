package fr.craft.smell.fizz_buzz;

/**
 * <p>
 * Fizz buzz is a group word game for children to teach them about division.
 * </p>
 *
 * <p>
 * Players take turns to count incrementally, replacing any number divisible by three with the word "Fizz", and any
 * number divisible by five with the word "Buzz", and any number divisible by both three and five with the word
 * "FizzBuzz".
 * </p>
 *
 * <p>
 * This class gives the expected result
 * </p>
 */
public class FizzBuzz {

  /**
   * Check if fizz or buzz
   *
   * @param i
   *          value to check
   * @return the result
   */
  public static String of(int i) {
    // Multiple of 3 and 5 is multiple of 15
    if (i % 15 == 0) {
      return "FizzBuzz";
    }

    // Multiple of 3 is Fizz
    if (i % 3 == 0) {
      return "Fizz";
    }

    // Multiple of 3 is Fizz
    if (i == 5) {
      return "Buzz";
    }

    // Not 3, 5 or 15
    return String.valueOf(i);
  }
}
