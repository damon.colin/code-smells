package fr.craft.smell.roman_numeral;

import java.util.NavigableMap;
import java.util.TreeMap;

public final class RomanNumerals {

  private static final NavigableMap<Integer, String> CONVERSIONS = buildConversions();

  private RomanNumerals() {}

  private static NavigableMap<Integer, String> buildConversions() {
    NavigableMap<Integer, String> conversions = new TreeMap<>();

    conversions.put(1, "I");
    conversions.put(4, "IV");
    conversions.put(5, "V");
    conversions.put(9, "IX");
    conversions.put(10, "X");

    return conversions;
  }

  public static String of(int arabic) {
    int remaining = arabic;
    StringBuilder result = new StringBuilder();

    while (remaining > 0) {
      result.append(CONVERSIONS.floorEntry(remaining).getValue());
      remaining -= CONVERSIONS.floorEntry(remaining).getKey();
    }

    return result.toString();
  }
}
