package fr.craft.smell.employee_report;

import static org.assertj.core.api.Assertions.*;

import fr.craft.smell.UnitTest;
import org.junit.jupiter.api.Test;

@UnitTest
class EmployeeReportTest {

  @Test
  void shouldGetEmployeeReport() {
    assertThat(employees().sundayWorkers()).containsExactly("Sepp", "Pierre-Étienne");
  }

  private Employees employees() {
    return Employees.builder().add("Max", 17).add("SEPP", 18).add("Nina", 15).add("pierre-étienne", 51).build();
  }
}
