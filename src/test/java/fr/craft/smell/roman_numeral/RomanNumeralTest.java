package fr.craft.smell.roman_numeral;

import static org.assertj.core.api.Assertions.*;

import fr.craft.smell.UnitTest;
import org.junit.jupiter.api.Test;

@UnitTest
class RomanNumeralTest {

  @Test
  void testZero() {
    assertThat(RomanNumerals.of(0)).isEqualTo("");
  }

  @Test
  void testOne() {
    assertThat(RomanNumerals.of(1)).isEqualTo("I");
  }

  @Test
  void testTwo() {
    assertThat(RomanNumerals.of(2)).isEqualTo("II");
  }

  @Test
  void testFour() {
    assertThat(RomanNumerals.of(4)).isEqualTo("IV");
  }

  @Test
  void testFive() {
    assertThat(RomanNumerals.of(5)).isEqualTo("V");
  }

  @Test
  void testSix() {
    assertThat(RomanNumerals.of(6)).isEqualTo("VI");
  }

  @Test
  void testSeven() {
    assertThat(RomanNumerals.of(7)).isEqualTo("VII");
  }
}
